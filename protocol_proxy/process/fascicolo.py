from __future__ import annotations
from settings import DEBUG
from datetime import datetime
from typing import List
from rest.rest_client import RestClient
from models.fascicolo import Fascicolo, InserisciInFascicolo
from models.anagrafica import RiferimentiAnagrafici
from models.ufficio import Ufficio, Utente
from models.protocollo import Classificazione
from models.excepitions import FascicoloNotCreated
from models.types import FascicoloType
from models.logger import get_logger
from configuration.utils import get_storage_fascicoli, save_storage_fascicolo

log = get_logger()


def get_fascicolo_to_insert(
    configuration: dict,
    classification: Classificazione,
    client: RestClient,
    remote_id: str,
    subject: str,
    office_list: List[Ufficio],
    anagrafica_list: List[RiferimentiAnagrafici],
) -> InserisciInFascicolo:
    fascicolo_configuration = _get_configuration_fascicolo(
        configuration,
        classification,
        client,
        remote_id,
        subject,
        office_list,
        anagrafica_list,
    )
    if fascicolo_configuration["progDoc"] and fascicolo_configuration["progMovi"]:
        fascicolo_type = FascicoloType.IDENTIFICATIVO
    elif fascicolo_configuration["anno"] and ["numero"]:
        fascicolo_type = FascicoloType.ESTREMI
    else:
        raise FascicoloNotCreated()
    return InserisciInFascicolo(
        codiceUfficio=fascicolo_configuration["codiceUfficio"],
        codiceRegistro=classification.codice,
        anno=fascicolo_configuration["anno"],
        numero=fascicolo_configuration["numero"],
        progDoc=fascicolo_configuration["progDoc"],
        progMovi=fascicolo_configuration["progMovi"],
        fascicolo_type=fascicolo_type,
    )


def _get_configuration_fascicolo(
    configuration: dict,
    classification: Classificazione,
    client: RestClient,
    remote_id: str,
    subject: str,
    office_list: List[Ufficio],
    anagrafica_list: List[RiferimentiAnagrafici],
) -> dict:
    try:
        fascicolo_configuration = {
            "codiceUfficio": configuration["office_code"],
            "codiceRegistro": classification.to_dict(),
            "dataApertura": datetime.now().strftime("%Y-%m-%d"),
        }
        log.debug(f"Configuration: {configuration}")
        if (configuration["folder_number"] and configuration["folder_year"]) or (
            configuration["prog_movi"] and configuration["prog_doc"]
        ):
            fascicolo_configuration["anno"] = configuration["folder_year"] or ""
            fascicolo_configuration["numero"] = configuration["folder_number"] or ""
            fascicolo_configuration["progDoc"] = configuration["prog_doc"] or ""
            fascicolo_configuration["progMovi"] = configuration["prog_movi"] or ""
        else:
            fascicoli = {}
            fascicoli = get_storage_fascicoli()
            log.debug(f"Fascicoli: {fascicoli}")
            if fascicoli and remote_id in fascicoli:
                fascicolo = fascicoli[remote_id]
                fascicolo_configuration["anno"] = fascicolo["anno"]
                fascicolo_configuration["numero"] = fascicolo["numero"]
                fascicolo_configuration["progDoc"] = fascicolo["progDoc"]
                fascicolo_configuration["progMovi"] = fascicolo["progMovi"]
            else:

                fascicolo_configuration = _create_fascicolo(
                    configuration["user_code"],
                    configuration["operating_office_code"],
                    subject,
                    anagrafica_list,
                    office_list,
                    client,
                    fascicolo_configuration,
                )
                log.debug(f"Fascicolo creato: {fascicolo_configuration}")
                fascicoli[remote_id] = {
                    "anno": fascicolo_configuration["anno"],
                    "numero": fascicolo_configuration["numero"],
                    "progDoc": fascicolo_configuration["progDoc"],
                    "progMovi": fascicolo_configuration["progMovi"],
                }
                save_storage_fascicolo(fascicoli)
        return fascicolo_configuration
    except Exception as e:
        log.error(f"errore di configurazione per fascicoli: {e}", exc_info=DEBUG)
        raise e


def _create_fascicolo(
    user_code: str,
    operating_office_code: str,
    subject: str,
    anagrafica_list: List[RiferimentiAnagrafici],
    office_list: List[Ufficio],
    client: RestClient,
    fascicolo_configuration: dict,
) -> dict:
    user = Utente(user_code)
    if "anno" not in fascicolo_configuration:
        fascicolo_configuration["anno"] = datetime.now().strftime("%Y")
    new_fascicolo = Fascicolo(
        subject,
        operating_office_code,
        user,
        fascicolo_configuration["codiceUfficio"],
        fascicolo_configuration["codiceRegistro"],
        fascicolo_configuration["anno"],
        fascicolo_configuration["dataApertura"],
        office_list,
        anagrafica_list,
    )
    return client.apertura_fascicolo(new_fascicolo)
