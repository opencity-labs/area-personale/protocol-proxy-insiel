from __future__ import annotations
from typing import List
from datetime import datetime
from process.authors_and_offices import create_anagrafica_list
from process.fascicolo import get_fascicolo_to_insert
from process.authors_and_offices import get_configuration_office_list
from models.logger import get_logger
from models.protocollo import Classificazione, OggettoDocumento, Protocollo
from models.ufficio import Utente
from models.types import TipoProtocollo, TrasmissionType
from models.anagrafica import RiferimentiAnagrafici
from models.documento import Documento
from models.fascicolo import InserisciInFascicolo
from rest.rest_client import RestClient

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


def process_message_for_create_protocollo(
    message: dict,
    main_document: Documento,
    list_attachemts: List[Documento],
    configuration: dict,
    client: RestClient,
):
    document_list: List[Documento] = [main_document]
    for document in list_attachemts:
        document_list.append(document)

    transmission_type = message["registration_data"]["transmission_type"]
    response = process_message_protocollo(
        message,
        document_list,
        configuration,
        transmission_type,
        client,
    )
    message["registration_data"]["date"] = str(
        datetime.now().replace(microsecond=0).astimezone().isoformat()
    )
    message["registration_data"]["document_number"] = str(response["registrazioni"][0]["numero"])
    message["folder"]["id"] = "n/a"
    log.info(f"Messaggio protocollato con successo {response}")
    return response


def process_message_protocollo(
    message: dict,
    documents: List[Documento],
    configuration: dict,
    transmission_type: str,
    client: RestClient,
) -> dict:
    log.debug("Inizio processamento protocollo")
    if transmission_type == TrasmissionType.INBOUND.value:
        tipo_protocollo = TipoProtocollo.ARRIVO.value
    else:
        tipo_protocollo = TipoProtocollo.PARTENZA.value
    # protocollo il messaggio
    user = Utente(configuration["user_code"])
    classification = Classificazione(configuration["register_code"])
    anagrafica_list: List[RiferimentiAnagrafici] = create_anagrafica_list(
        message["author"], user, client
    )
    office_list = get_configuration_office_list(configuration["internal_offices"])
    fascicolo: InserisciInFascicolo = get_fascicolo_to_insert(
        configuration,
        classification,
        client,
        message["remote_id"],
        message["folder"]["title"],
        office_list,
        anagrafica_list,
    )

    log.debug("Creazione protocollo")
    # trasforma il json in dict
    oggetto_documento = OggettoDocumento(message["main_document"]["description"])

    protocollo = Protocollo(
        user,
        configuration["operating_office_code"],
        configuration["office_code"],
        configuration["office_registry"],
        tipo_protocollo,
        oggetto_documento,
        anagrafica_list,
        office_list,
        documents,
        classification,
        fascicolo,
    )
    return client.inserisci_protocollo(protocollo)
