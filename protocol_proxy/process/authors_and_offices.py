from __future__ import annotations
from typing import List

from models.logger import get_logger
from models.ufficio import Ufficio, Utente
from models.anagrafica import Anagrafica, Email, RiferimentiAnagrafici
from rest.rest_client import RestClient

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


def create_anagrafica_list(
    authors: List[dict], user: Utente, client: RestClient
) -> List[RiferimentiAnagrafici]:
    log.debug("Creazione anagrafica")
    anagrafica_list: List[RiferimentiAnagrafici] = []
    for author in authors:
        # crea l'anagrafica
        email = Email(author["email"])
        anagrafica_to_send = Anagrafica(
            utente=user,
            cognome=author["family_name"],
            nome=author["name"],
            codiceFiscale=author["tax_identification_number"],
            indirizzo=author["street_name"],
            comune=author.get("comune"),
            provincia=author.get("provincia"),
            cap=author["postal_code"],
            email=email,
        )
        response = client.predisponi_anagrafica(anagrafica_to_send)
        anagrafica = RiferimentiAnagrafici(response["anagrafica"]["descAna"])
        anagrafica_list.append(anagrafica)
    return anagrafica_list


def get_configuration_office_list(conf_internal_office: List[dict]) -> List[Ufficio]:
    log.debug("Creazione lista uffici")
    office_list = []
    for office_conf in conf_internal_office:
        office = Ufficio(office_conf["code"], office_conf["type"])
        office_list.append(office)
    log.debug(f"Lista uffici nella configurazione:{office_list}")
    return office_list
