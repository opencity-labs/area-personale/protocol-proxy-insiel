from __future__ import annotations
import hashlib
import filetype
from settings import DEBUG
from typing import List, Union
from models.documento import Documento
from models.auth_token import AuthClient
from models.excepitions import MainDocumentException, ProtocolloException
from models.logger import get_logger
from models.types import FlowState
from rest.rest_client import RestClient

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


def process_attachments_and_main_document(
    message: dict,
    client: RestClient,
    flow_message: FlowState,
) -> Union[List[dict], Documento, List[Documento]]:
    processed_attachment: List[dict] = []
    list_attachemts: List[Documento] = []
    try:
        if flow_message == FlowState.PARTIAL_REGISTRATION:
            for attachment in message["retry_meta"]["attachments"]:
                processed_attachment.append(attachment)
            # cerca nella lista degli allegati il documento principale
            main_document = _find_main_document(processed_attachment)
            list_attachemts = _find_attachment_list(processed_attachment)
        else:
            # carica il documento principale
            main_document: Documento = _upload_main_document(message, client)
            dict_main_document = main_document.to_dict()
            dict_main_document["url"] = message["main_document"]["url"]
            # aggiunge il dizionario al dizionario allegati
            processed_attachment.append(dict_main_document)

        # processa la lista degli allegati se non sono stati processati tutti
        not_processed_list_attachments = message["attachments"]
        if len(list_attachemts) != len(not_processed_list_attachments):
            processed_attachment, list_attachemts = _process_attachment_list(
                not_processed_list_attachments,
                processed_attachment,
                client,
                message["tenant_id"],
            )

        if not main_document:
            raise ProtocolloException()

        return processed_attachment, main_document, list_attachemts
    except MainDocumentException:
        log.error(
            f"Errore durante il caricamento del documento principale", exc_info=DEBUG
        )
        return processed_attachment, None, None
    except ProtocolloException:
        log.error(f"Errore durante il processamento degli allegati", exc_info=DEBUG)
        return processed_attachment, None, None
    except Exception as e:
        log.error(f"Errore durante il processamento dei documenti: {e}", exc_info=DEBUG)
        return processed_attachment, None, None


def _find_attachment_list(processed_attachment: List[dict]) -> List[Documento]:
    list_attachments: List[Documento] = []
    for attachment in processed_attachment:
        if not attachment["primario"]:
            document = Documento(
                attachment["id"], attachment["nome"], attachment["primario"]
            )
            list_attachments.append(document)
    return list_attachments


def _find_main_document(processed_attachment: List[dict]) -> Documento:
    for attachment in processed_attachment:
        if attachment["primario"]:
            return Documento(
                attachment["id"], attachment["nome"], attachment["primario"]
            )
    raise MainDocumentException()


def _upload_main_document(message: dict, client: RestClient) -> Documento:
    log.debug(
        f'Caricamento documento principale scaricato da {message["main_document"]["url"]}'
    )
    main_document_downloaded = _download_pdf(
        message["main_document"]["url"], message["tenant_id"]
    )

    if main_document_downloaded is None:
        log.error(
            f"Errore durante il caricamento del documento principale", exc_info=DEBUG
        )
        raise MainDocumentException()
    kind = filetype.guess(main_document_downloaded)
    mime_type = kind.mime
    log.debug(f"MIME type del contenuto: {mime_type}")
    main_document = _upload_attachment(
        message["main_document"],
        mime_type,
        main_document_downloaded,
        client,
        True,
    )

    return main_document


def _process_attachment_list(
    attachments: List[dict],
    processed_attachment: List[dict],
    client: RestClient,
    tenant_id: str,
) -> Union[List[dict], List[Documento]]:
    """
    Processa la lista di allegati.
    """
    log.debug(f"Caricamento allegati nel messaggio: {attachments}")
    log.debug(f"Allegati gia caricati: {processed_attachment}")
    try:
        list_attachments: List[Documento] = []
        for attachment in attachments:
            # cerca nella lista dei allegati gia caricati se l'allegato esiste
            if _find_attachment_in_list(attachment, processed_attachment):
                continue
            # Estrai i dati dell'allegato
            url = attachment["url"]
            # Esegui il processamento dell'allegato
            file_base64 = _download_pdf(url, tenant_id)
            if file_base64 is None:
                raise ProtocolloException(
                    f"Errore durante il caricamento dell'allegato {attachment['name']}"
                )
            else:
                kind = filetype.guess(file_base64)
                mime_type = kind.mime
                log.debug(f"MIME type del contenuto: {mime_type}")
                document = _upload_attachment(
                    attachment, mime_type, file_base64, client, False
                )
                log.debug(f"Creato Allegato:{document}")
                list_attachments.append(document)
                document_dict = document.to_dict()
                document_dict["url"] = url
                processed_attachment.append(document_dict)
        # controlla che la lista di allegati sia vuota
        return processed_attachment, list_attachments
    except ProtocolloException as pe:
        log.error(pe, exc_info=DEBUG)
        return processed_attachment, None
    except Exception as e:
        log.error(f"Errore durante il caricamento degli allegati: {e}", exc_info=DEBUG)
        return processed_attachment, None


def _find_attachment_in_list(
    attachment: dict, processed_attachment: List[dict]
) -> bool:
    for document_uploaded in processed_attachment:
        if (
            document_uploaded["nome"] == attachment["name"]
            and document_uploaded["url"] == attachment["url"]
        ):
            return True
    return False


def _upload_attachment(
    attachment: dict,
    mime_type: str,
    file_base64: bytes,
    client: RestClient,
    first_document: bool,
) -> Documento:
    name = attachment["name"]
    md5 = attachment["md5"]
    if not md5 or md5 == "":
        md5 = hashlib.md5(file_base64).hexdigest()
    document_uploaded = client.upload_file(name, mime_type, file_base64, md5)
    log.debug(f"Allegato {name} caricato correttamente")
    attachment = Documento(document_uploaded["idFile"], name, first_document)
    return attachment


def _download_pdf(
    url: str,
    tenant_id: str,
) -> str:
    auth_client = AuthClient(tenant_id)
    try:
        # Effettua una richiesta GET per scaricare il contenuto del PDF dalla URL
        log.debug(f"Richiesta GET per scaricare il contenuto del PDF dalla URL: {url}")
        response = auth_client.download_file_with_auth(url)
        # Verifica se la richiesta è stata eseguita con successo (codice 200)
        if response.status_code == 200:
            # Ottieni il contenuto del PDF
            return response.content
        else:
            # Se la richiesta non è andata a buon fine, stampa un messaggio di errore
            log.error(
                f"Errore nella richiesta. Codice: {response.status_code}",
                exc_info=DEBUG,
            )
            return None
    except Exception as e:
        # Se si verifica un errore durante la richiesta, stampa un messaggio di errore
        log.error(f"Errore durante la richiesta GET: {e}", exc_info=DEBUG)
        return None
