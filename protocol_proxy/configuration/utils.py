import json
from models.logger import get_logger
from storage.storage_manager import StorageManager
from settings import DEBUG

log = get_logger()
storage_manager = StorageManager()


def read_local_schema():
    try:
        with open("./configuration/schema.json", "r") as file:
            return file.read()
    except Exception as e:
        log.error(f"Error reading schema: {e}", exc_info=DEBUG)
        return None


def save_configuration(dicionary_configuration: dict):
    try:
        file_path = f"{dicionary_configuration['id']}.json"
        if not storage_manager.save(file_path, dicionary_configuration):
            raise Exception("Error saving configuration")
        log.debug(f"Configuration saved in: {file_path}")
    except Exception as e:
        log.error(f"Error saving configuration: {e}", exc_info=DEBUG)


def check_configuration(key: str) -> dict:
    try:
        conf_services = storage_manager.read(f"{key}.json")
        log.debug(f"Configuration read from: {key}.json")
        return json.loads(conf_services)
    except Exception as e:
        log.warning(f"Error reading configuration: {e}", exc_info=DEBUG)
        return None


def delete_configuration(key: str) -> bool:
    try:
        response = storage_manager.delete(f"{key}.json")
        if response:
            log.debug(f"Configuration deleted from: {key}.json")
        else:
            log.error(f"Configuration not deleted from: {key}.json", exc_info=DEBUG)
        return response
    except Exception as e:
        log.error(f"Error deleting configuration: {e}", exc_info=DEBUG)
        return False


def save_storage_fascicolo(dicionary_configuration: dict):
    try:
        file_path = "storage_fascicoli.json"
        if not storage_manager.save(file_path, dicionary_configuration):
            raise Exception("Error saving configuration")
        log.debug(f"Configuration saved in: {file_path}")
    except Exception as e:
        log.error(f"Error saving configuration: {e}", exc_info=DEBUG)


def get_storage_fascicoli() -> dict:
    try:
        conf_services = storage_manager.read("storage_fascicoli.json")
        if conf_services is None:
            conf_services = {
                "storage": {"anno": 0, "numero": 0, "progDoc": 0, "progMovi": 0}
            }
            save_storage_fascicolo(conf_services)
        else:
            conf_services = json.loads(conf_services)
        log.debug("Configuration read from: storage_fascicoli.json")
        return conf_services
    except Exception as e:
        log.error(f"Error reading configuration: {e}", exc_info=DEBUG)
        return None
