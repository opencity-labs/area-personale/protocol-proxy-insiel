import json
import requests
import uuid

BASE_URL = "http://localhost:8000"

# generate random ids
new_tenant_to_create = str(uuid.uuid4())
tenant_to_not_find = str(uuid.uuid4())
new_service_to_create = str(uuid.uuid4())
service_to_not_find = str(uuid.uuid4())


# Test payloads with simplified IDs
tenant_payload = {
    "id": "test-tenant-id",  # Simplified tenant ID
    "description": "Random Tenant Description",
    "slug": "random-tenant-slug",
    "base_url": "https://sdc.example.com",
    "institution_code": "random-institution-code",
    "aoo_code": "random-aoo-code",
    "created_at": "2023-06-05",
    "modified_at": "2023-08-07",
}

with open("tests/configuration.json", "r") as file:
    service_payload = json.load(file)


# Test GET Endpoints
def test_get_metrics():
    response = requests.get(f"{BASE_URL}/metrics")
    assert response.status_code == 200


def test_get_status():
    response = requests.get(f"{BASE_URL}/status")
    assert response.status_code == 200


def test_get_schema():
    response = requests.get(f"{BASE_URL}/schema")
    assert response.status_code == 200


# Test POST for creating tenants
def test_create_not_existing_tenant():
    tenant_payload["id"] = new_tenant_to_create
    response = requests.post(f"{BASE_URL}/tenants/", json=tenant_payload)
    assert response.status_code == 201


def test_create_existing_tenant():
    tenant_payload["id"] = new_tenant_to_create
    response = requests.post(f"{BASE_URL}/tenants/", json=tenant_payload)
    assert response.status_code == 409


# Test GET for retrieving non-existing tenant
def test_get_not_existing_tenant():
    response = requests.get(f"{BASE_URL}/tenants/{tenant_to_not_find}")
    assert response.status_code == 404


# Test PUT for updating tenants
def test_update_existing_tenant():
    tenant_payload["id"] = new_tenant_to_create
    response = requests.put(
        f"{BASE_URL}/tenants/{new_tenant_to_create}", json=tenant_payload
    )
    assert response.status_code == 201


def test_update_not_existing_tenant():
    tenant_payload["id"] = tenant_to_not_find
    response = requests.put(
        f"{BASE_URL}/tenants/{tenant_to_not_find}", json=tenant_payload
    )
    assert response.status_code == 404


# Test PATCH for updating tenants
def test_patch_existing_tenant():
    tenant_payload["id"] = new_tenant_to_create
    response = requests.patch(
        f"{BASE_URL}/tenants/{new_tenant_to_create}", json=tenant_payload
    )
    assert response.status_code == 201


def test_patch_not_existing_tenant():
    tenant_payload["id"] = tenant_to_not_find
    response = requests.patch(
        f"{BASE_URL}/tenants/{tenant_to_not_find}", json=tenant_payload
    )
    assert response.status_code == 404


# Test DELETE for deleting tenants
def test_delete_not_existing_tenant():
    response = requests.delete(f"{BASE_URL}/tenants/{tenant_to_not_find}")
    assert response.status_code == 404


def test_delete_existing_tenant():
    response = requests.delete(f"{BASE_URL}/tenants/{new_tenant_to_create}")
    assert response.status_code == 204


# Test POST per creare un servizio non esistente
def test_create_not_existing_service():
    service_payload["id"] = new_service_to_create
    response = requests.post(f"{BASE_URL}/services/", json=service_payload)
    assert response.status_code == 201


# Test POST per creare un servizio già esistente
def test_create_existing_service():
    service_payload["id"] = new_service_to_create
    response = requests.post(f"{BASE_URL}/services/", json=service_payload)
    assert response.status_code == 409


# Test GET per recuperare un servizio non esistente
def test_get_not_existing_service():
    response = requests.get(f"{BASE_URL}/services/{service_to_not_find}")
    assert response.status_code == 404


# Test GET per recuperare un servizio esistente
def test_get_existing_service():
    response = requests.get(f"{BASE_URL}/services/{new_service_to_create}")
    assert response.status_code == 200


# Test PUT per aggiornare un servizio esistente
def test_update_existing_service():
    service_payload["id"] = new_service_to_create
    response = requests.put(
        f"{BASE_URL}/services/{new_service_to_create}", json=service_payload
    )
    assert response.status_code == 201


# Test PUT per aggiornare un servizio non esistente
def test_update_not_existing_service():
    service_payload["id"] = service_to_not_find
    response = requests.put(
        f"{BASE_URL}/services/{service_to_not_find}", json=service_payload
    )
    assert response.status_code == 404


# Test PATCH per aggiornare un servizio esistente
def test_patch_existing_service():
    service_payload["id"] = new_service_to_create
    response = requests.patch(
        f"{BASE_URL}/services/{new_service_to_create}", json=service_payload
    )
    assert response.status_code == 201


# Test PATCH per aggiornare un servizio non esistente
def test_patch_not_existing_service():
    service_payload["id"] = service_to_not_find
    response = requests.patch(
        f"{BASE_URL}/services/{service_to_not_find}", json=service_payload
    )
    assert response.status_code == 404


# Test DELETE per cancellare un servizio non esistente
def test_delete_not_existing_service():
    response = requests.delete(f"{BASE_URL}/services/{service_to_not_find}")
    assert response.status_code == 404


# Test DELETE per cancellare un servizio esistente
def test_delete_existing_service():
    response = requests.delete(f"{BASE_URL}/services/{new_service_to_create}")
    assert response.status_code == 204
