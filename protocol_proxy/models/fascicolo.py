from __future__ import annotations
from typing import List, Optional
from models.ufficio import Ufficio, Utente
from models.anagrafica import RiferimentiAnagrafici
from models.types import FascicoloType


class InserisciInFascicolo:

    def __init__(
        self,
        codiceUfficio: str,
        codiceRegistro: str,
        anno: Optional[int],
        numero: Optional[int],
        progDoc: Optional[int],
        progMovi: Optional[int],
        fascicolo_type: FascicoloType,
    ):
        self.codiceUfficio = codiceUfficio
        self.codiceRegistro = codiceRegistro
        self.anno = anno
        self.numero = numero
        self.progDoc = progDoc
        self.progMovi = progMovi
        self.fascicolo_type = fascicolo_type

    def to_dict(self):
        if self.fascicolo_type == FascicoloType.ESTREMI:
            return {
                "estremi": {
                    "codiceUfficio": self.codiceUfficio,
                    "codiceRegistro": self.codiceRegistro,
                    "anno": self.anno,
                    "numero": self.numero,
                    "subNumero": " ",
                }
            }
        else:
            return {
                "id": {
                    "progDoc": self.progDoc,
                    "progMovi": self.progMovi,
                }
            }


class Fascicolo:

    def __init__(
        self,
        oggetto: str,
        codiceUfficioOperante: Optional[str],
        utente: Utente,
        codiceUfficio: str,
        codiceRegistro: dict,
        anno: int,
        data: str,
        uffici: List[Ufficio],
        riferimentiAnagrafici: List[RiferimentiAnagrafici],
    ):
        self.utente = utente
        self.codiceUfficioOperante = codiceUfficioOperante
        self.codiceUfficio = codiceUfficio
        self.codiceRegistro = codiceRegistro
        self.oggetto = oggetto
        self.anno = anno
        self.data = data
        self.uffici = uffici
        self.riferimentiAnagrafici = riferimentiAnagrafici

    def to_dict(self) -> dict:
        """
        Converte l'istanza di Protocollo in formato JSON.
        """
        return {
            "utente": self.utente.to_dict(),
            "codiceUfficioOperante": self.codiceUfficioOperante,
            "codiceUfficio": self.codiceUfficio,
            "codiceRegistro": self.codiceRegistro,
            "oggetto": self.oggetto,
            "anno": self.anno,
            "data": self.data,
            "uffici": [ufficio.to_dict() for ufficio in self.uffici],
            "riferimentiAnagrafici": [
                riferimenti.to_dict() for riferimenti in self.riferimentiAnagrafici
            ],
        }
