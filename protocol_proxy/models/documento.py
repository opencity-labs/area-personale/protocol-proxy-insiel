from __future__ import annotations


class Documento:
    def __init__(self, id: str, nome: str, primario: bool):
        self.id = id
        self.nome = nome
        self.primario = primario

    def to_dict(self) -> dict:
        return {
            "id": self.id,
            "nome": self.nome,
            "primario": self.primario,
            "inviaIOP": True,
        }
