from __future__ import annotations
from typing import Optional


class Ufficio:

    def __init__(self, codice: str, tipo: Optional[str]):
        self.codice = codice
        if tipo == "-":
            tipo = None
        self.tipo = tipo

    def to_dict(self) -> dict:
        return {"codice": self.codice, "tipo": self.tipo}


class Utente:
    def __init__(self, codice: str):
        self.codice = codice

    def to_dict(self) -> dict:
        return {"codice": self.codice, "codiceFiscale": None}
