class ProtocolloException(Exception):
    pass


class JsonException(Exception):
    pass


class ExistException(Exception):
    pass


class NotExistingException(Exception):
    pass


class InsielException(Exception):
    pass


class MainDocumentException(Exception):
    pass


class FascicoloNotCreated(Exception):
    pass
