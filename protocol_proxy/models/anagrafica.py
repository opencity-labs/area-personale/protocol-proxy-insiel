from __future__ import annotations
from typing import List, Optional
from models.ufficio import Utente
from models.types import TypeEmail


class RiferimentiAnagrafici:

    def __init__(self, descrizione: str):
        self.descrizione = descrizione

    def to_dict(self):
        return {"descrizione": self.descrizione}


class Email:
    def __init__(self, email: str):
        self.email = email

    def to_dict(self) -> dict:
        return {"email": self.email, "tipo": TypeEmail.EMAIL.value, "principale": True}


class Anagrafica:
    def __init__(
        self,
        utente: Utente,
        cognome: str,
        nome: str,
        codiceFiscale: str,
        indirizzo: str,
        comune: str,
        provincia: str,
        cap: str,
        email: Email,
    ):
        self.utente = utente
        self.cognome = cognome
        self.nome = nome
        self.codiceFiscale = codiceFiscale
        self.indirizzo = indirizzo
        self.comune = comune
        self.provincia = provincia
        self.cap = cap
        self.emailList = [email]

    def to_dict(self) -> dict:
        return {
            "utente": self.utente.to_dict(),
            "anagrafica": {
                "denominazione": None,
                "cognome": self.cognome,
                "nome": self.nome,
                "codiceFiscale": self.codiceFiscale,
                "indirizzo": self.indirizzo,
                "comune": self.comune,
                "provincia": self.provincia,
                "cap": self.cap,
                "emailList": [email.to_dict() for email in self.emailList],
            },
        }
