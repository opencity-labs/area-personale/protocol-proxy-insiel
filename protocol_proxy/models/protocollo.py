from __future__ import annotations
from typing import List, Optional
from models.documento import Documento
from models.ufficio import Ufficio, Utente
from models.fascicolo import InserisciInFascicolo, RiferimentiAnagrafici
from models.types import TipoProtocollo


class OggettoDocumento:

    def __init__(self, oggetto: str):
        self.oggetto = oggetto

    def to_dict(self) -> dict:
        return {
            "oggetto": self.oggetto,
            "importo": None,
            "valuta": None,
        }


class Protocollo:

    def __init__(
        self,
        utente: Utente,
        codiceUfficioOperante: Optional[str],
        codiceUfficio: str,
        codiceRegistro: str,
        verso: str,
        oggettoDocumento: OggettoDocumento,
        anagrafica_list: List[RiferimentiAnagrafici],
        uffici: List[Ufficio],
        documenti: List[Documento],
        classifiche: Classificazione,
        inserisciInFascicolo: InserisciInFascicolo,
    ):
        self.utente = utente
        self.codiceUfficioOperante = codiceUfficioOperante
        self.codiceUfficio = codiceUfficio
        self.codiceRegistro = codiceRegistro
        self.verso = verso
        self.oggettoDocumento = oggettoDocumento
        list_of_anagrafica = [mittente.to_dict() for mittente in anagrafica_list]
        list_of_uffici = [{"codice": codiceUfficio}]
        if verso == TipoProtocollo.ARRIVO.value:
            self.mittenti = list_of_anagrafica
            self.destinatari = list_of_uffici
        else:
            self.mittenti = list_of_uffici
            self.destinatari = list_of_anagrafica
        self.uffici = uffici
        self.documenti = documenti
        self.classifiche = [classifiche]
        self.inserisciInFascicolo = [inserisciInFascicolo]

    def to_dict(self) -> dict:
        """
        Converte l'istanza di Protocollo in formato JSON.
        """
        return {
            "utente": self.utente.to_dict(),
            "codiceUfficioOperante": self.codiceUfficioOperante,
            "codiceUfficio": self.codiceUfficio,
            "codiceRegistro": self.codiceRegistro,
            "verso": self.verso,
            "oggettoDocumento": self.oggettoDocumento.to_dict(),
            "mittenti": self.mittenti,
            "destinatari": self.destinatari,
            "uffici": [ufficio.to_dict() for ufficio in self.uffici],
            "documenti": [doc.to_dict() for doc in self.documenti],
            "classifiche": [
                classificazione.to_dict() for classificazione in self.classifiche
            ],
            "inserisciInFascicolo": [
                inserimento.to_dict() for inserimento in self.inserisciInFascicolo
            ],
        }


class Classificazione:
    def __init__(self, codice: str):
        self.codice = codice

    def to_dict(self) -> dict:
        return {"codice": self.codice}
