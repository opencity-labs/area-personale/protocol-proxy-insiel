from __future__ import annotations
import json
import xmltodict
import requests
from settings import DEBUG
from requests.auth import HTTPBasicAuth
from models.logger import get_logger
from models.types import RestHeaderClient
from models.protocollo import Protocollo
from models.excepitions import InsielException
from models.anagrafica import Anagrafica
from models.fascicolo import Fascicolo

log = get_logger()


class RestClient(object):
    def __init__(self, configuration: dict) -> None:
        self.configuration = configuration

    def _raise_exception(self, response):
        try:
            xml_string = response.content.decode("utf-8")
            # Tenta di analizzare come XML
            response_dict = xmltodict.parse(xml_string)
        except xmltodict.expat.ExpatError:
            try:
                # Se fallisce, tenta di analizzare come JSON
                response_dict = json.loads(xml_string)
            except json.JSONDecodeError:
                # Se fallisce anche il parsing JSON, logga l'errore e alza l'eccezione
                log.error(
                    f"Error in post request, response content could not be parsed: {xml_string}",
                    exc_info=DEBUG,
                )
                raise InsielException(
                    f"Error in post request, status code: {response.status_code}"
                )

        log.error(f"Error in post request, response: {response_dict}", exc_info=DEBUG)
        raise InsielException(
            f"Error in post request, status code: {response.status_code}"
        )

    def _login(self) -> str:
        """Login to the API
        Raises:
            InsielException
        Returns:
            str: token di autenticazione
        """
        log.debug("Login")
        url = f"{self.configuration['login_url']}?grant_type={self.configuration['grant_type']}"
        auth = HTTPBasicAuth(
            self.configuration["client_user"], self.configuration["client_secret"]
        )
        response = requests.post(url, auth=auth)
        if response.status_code == 200:
            response = response.json()
            log.debug(f"Login successful, response: {response}")
            return response["access_token"]
        else:
            log.error(
                f"Login failed, response: {response} - {response.content}",
                exc_info=DEBUG,
            )
            raise InsielException("Login failed")

    def upload_file(
        self, file_name: str, mime_type: str, file_to_upload: bytes, md5: str
    ):
        """Upload file
        Args:
            file_to_upload (bytes): file da caricare
            md5 (str): md5 del file da caricare
        Raises:
            InsielException
        Returns:
            dict: response
        """
        try:
            log.debug("Upload file")
            token = self._login()
            url = f"{self.configuration['registry_url']}/upload-file"
            headers = {"Authorization": f"Bearer {token}"}
            payload = {"improntaMd5": md5}
            files = [("file", (file_name, file_to_upload, mime_type))]
            response = requests.post(url, headers=headers, data=payload, files=files)
            log.debug(f"Risposta file: {response}")
            if response.status_code == 200:
                return response.json()
            else:
                self._raise_exception(response)
        except InsielException as e:
            log.error(e, exc_info=DEBUG)

    def predisponi_anagrafica(self, anagrafica: Anagrafica):
        """Predisponi anagrafica
        Args:
            anagrafica (Anagrafica): anagrafica da predisponire
        Raises:
            InsielException
        Returns:
            dict: response
        """
        try:
            log.debug("Predisponi anagrafica")
            token = self._login()
            url = f"{self.configuration['registry_url']}/predisponi-anagrafica"
            headers = {
                "Content-Type": RestHeaderClient.APPLICATION_JSON.value,
                "Authorization": f"Bearer {token}",
            }
            response = requests.post(
                url, headers=headers, data=json.dumps(anagrafica.to_dict())
            )
            log.debug(f"Risposta anagrafica: {response}")
            if response.status_code == 200:
                return response.json()
            else:
                self._raise_exception(response)
        except InsielException as e:
            log.error(e, exc_info=DEBUG)

    def apertura_fascicolo(self, fascicolo: Fascicolo):
        """Apertura fascicolo
        Args:
            fascicolo (Fascicolo): fascicolo da apertura
        Raises:
            InsielException
        Returns:
            dict: response
        """
        try:
            log.debug("Apertura fascicolo")
            token = self._login()
            url = f"{self.configuration['registry_url']}/apertura-fascicolo"
            headers = {
                "Content-Type": RestHeaderClient.APPLICATION_JSON.value,
                "Authorization": f"Bearer {token}",
            }
            response = requests.post(
                url, headers=headers, data=json.dumps(fascicolo.to_dict())
            )
            log.debug(f"Risposta fascicolo: {response}")
            if response.status_code == 200:
                return response.json()
            else:
                self._raise_exception(response)
        except InsielException as e:
            log.error(e, exc_info=DEBUG)

    def inserisci_protocollo(self, protocollo: Protocollo):
        """Inserisci protocollo
        Args:
            protocollo (Protocollo): protocollo da inserire
        Raises:
            InsielException
        Returns:
            dict: response
        """
        try:
            log.debug("Inserisco protocollo")
            token = self._login()
            url = f"{self.configuration['registry_url']}/inserisci-protocollo"
            headers = {
                "Content-Type": RestHeaderClient.APPLICATION_JSON.value,
                "Authorization": f"Bearer {token}",
            }
            response = requests.post(
                url, headers=headers, data=json.dumps(protocollo.to_dict())
            )
            log.debug(f"Risposta protocollo: {response}")
            if response.status_code == 200:
                return response.json()
            else:
                self._raise_exception(response)
        except InsielException as e:
            log.error(e, exc_info=DEBUG)
