#!/bin/bash

# Aggiorna la lista dei pacchetti
apt-get update
apt-get install -qy netcat-openbsd wget unzip curl

# Installa rpk (Redpanda)
wget --quiet https://github.com/redpanda-data/redpanda/releases/latest/download/rpk-linux-amd64.zip
mkdir -p ~/.local/bin
export PATH="~/.local/bin:$PATH"
unzip rpk-linux-amd64.zip -d ~/.local/bin/
rpk topic create $KAFKA_TOPIC_NAME --brokers=$KAFKA_BOOTSTRAP_SERVERS

# Attendi che Kafka sia pronto
while ! nc -z kafka 9092; do
  echo "Waiting for Kafka..."
  sleep 1
done
echo "Kafka is ready"

# Scarica e installa MinIO Client (mc)
wget --quiet https://dl.min.io/client/mc/release/linux-amd64/mc
chmod +x mc
mv mc /usr/local/bin/

# Attendi che MinIO sia completamente avviato
while ! nc -z minio 9000; do
  echo "Waiting for MinIO to start..."
  sleep 1
done
echo "MinIO is ready"

mc alias set minio_test http://minio:9000 $STORAGE_ACCESS_S3_KEY $STORAGE_KEY_S3_ACCESS_SECRET
mc admin user add minio_test $STORAGE_ACCESS_S3_KEY $STORAGE_KEY_S3_ACCESS_SECRET;
mc admin policy attach minio_test readwrite --user=$STORAGE_ACCESS_S3_KEY
mc mb --ignore-existing minio_test/protocollo;
mc policy set download minio_test/protocollo $STORAGE_ACCESS_S3_KEY;
mc policy set upload minio_test/protocollo $STORAGE_ACCESS_S3_KEY;

# Configura e attiva l'ambiente virtuale Python
python -m venv venv
source venv/bin/activate

# Aggiorna pip e installa i pacchetti necessari
pip install --quiet --upgrade pip
pip install --quiet --no-cache-dir -r protocol_proxy/requirements.txt

# Avvia il server FastAPI
cd protocol_proxy
uvicorn main_api:app --host 0.0.0.0 --port 8000 --log-level debug &

# Attendi che il server FastAPI sia pronto
echo "Waiting for FastAPI server to be ready..."
while ! nc -z localhost 8000; do
  echo "Waiting for FastAPI..."
  sleep 1
done
echo "FastAPI is ready"
